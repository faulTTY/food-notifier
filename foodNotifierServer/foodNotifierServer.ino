/*
 * This Arduino program creates a web server using the ESP8266 core that responds to HTTP GET requests. It includes a button connected to a specified pin,
 * and the server returns a JSON object containing the current status (toggled by pressing the button) when a GET request is made. The program utilizes the ArduinoJson
 * library for JSON serialization and provides a simple debounce mechanism to prevent erratic button readings. The server runs on port 80, and users can access the status 
 * by sending a GET request to the root ("/") endpoint.
 */

#include "arduino_secrets.h"
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
// #include <ESP8266HTTPClient.h>
// #include <WiFiClientSecure.h>

// Global variables and objects
const char ssid[] = SECRET_SSID;
const char pass[] = SECRET_PASS;
String newHostname = "foodNotifier";

const int pullDown = D8;

const int buttonPin = D1;  // Change this to the pin where your button is connected
const int debounceDelay = 50;  // Adjust the debounce delay as needed

int buttonState = HIGH;  // Assuming the button is normally pulled HIGH
int lastButtonState = HIGH;
bool status = false;  // Initial state of the boolean variable
unsigned long lastDebounceTime = 0;

ESP8266WebServer server(80);

void setup() {
  // Serial
  Serial.begin(115200);
  Serial.println("\n\n");

  // Pins
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pullDown, OUTPUT);
  
  pinMode(buttonPin, INPUT);
  
  digitalWrite(LED_BUILTIN, 1);
  digitalWrite(pullDown, 0);

  // Wifi
  WiFi.hostname(newHostname.c_str());
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("\nConnected! IP address: " + WiFi.localIP().toString());

  // Set up the web server routes
  server.on("/", HTTP_GET, handleRoot);

  // Start the server
  server.begin();
}

void loop() {
  // Wait for WiFi connection
  if (WiFi.status() == WL_CONNECTED) {
    
    int reading = digitalRead(buttonPin);

    // Check if the button state has changed
    if (reading != lastButtonState) {
      lastDebounceTime = millis();
    }

    // Check if the button state has been stable for the debounce delay
    if ((millis() - lastDebounceTime) > debounceDelay) {
      // Update the button state only if it has been stable
      if (reading != buttonState) {
        buttonState = reading;

        // Toggle the boolean status variable when the button is pressed
        if (buttonState == LOW) {
          status = !status;
          digitalWrite(LED_BUILTIN, !status);

          // Optional: Print the updated status to Serial Monitor
          Serial.print("Status: ");
          Serial.println(status);
        }
      }
    }

    // Save the current button state for the next iteration
    lastButtonState = reading;

    // Handle web server requests
    server.handleClient();
  }
}

void handleRoot() {
  // Create a JSON object
  StaticJsonDocument<200> jsonDoc;
  jsonDoc["status"] = status;

  // Serialize JSON to a String
  String jsonString;
  serializeJson(jsonDoc, jsonString);

  // Send the JSON response
  server.send(200, "application/json", jsonString);
}
