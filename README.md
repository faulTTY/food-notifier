# Food Notifier

Food Notifier is an Arduino-based project that uses the ESP8266WiFi module to give the cook in a household the ability to remotely let other people know that the food is ready.

For this, the project is split into two categories:
- [Food Notifier Server](#food-notifier-server) (Limited to 1)
- [Food Notifier Client](#food-notifier-client) (Unlimited number)

### Step 1 - Meal preparation is finished - Family member is working
<img src="comic/comic_1.jpeg" width="40%"> <img src="comic/comic_3.jpeg" width="40%">

### Step 2 - Press button on [Food Notifier Server](#food-notifier-server) - Green light on [Food Notifier Client](#food-notifier-client) indicates that dinner is ready
<img src="comic/comic_2.jpeg" width="40%"> <img src="comic/comic_4.jpeg" width="40%">


## Food Notifier Server

The Food Notifier Server utilizes the ESP8266 core to create a web server that responds to HTTP GET requests. The server includes a button connected to a specified pin, and it returns a JSON object containing the current status (toggled by pressing the button) when a GET request is made. The program incorporates the ArduinoJson library for JSON serialization and implements a simple debounce mechanism to prevent erratic button readings.

### Dependencies

- ArduinoJson: A powerful library for serializing and deserializing JSON data.

### Hardware Requirements

- ESP8266 board
- Button connected to a specified pin (default: D1)
- Pull-down resistor connected to a specified pin (default: D8)

### Configuration

- Modify the arduino_secrets.h file to include your WiFi credentials. Replace SECRET_SSID and SECRET_PASS with your WiFi network name (SSID) and password.
- Adjust the buttonPin variable to the pin where your button is connected.
- Customize the debounceDelay variable to set the debounce delay as needed.

### Usage

- Upload the code to your ESP8266 board using the Arduino IDE or a compatible development environment.
- Open the Serial Monitor to view the IP address assigned to the ESP8266.
- Connect to the WiFi network named "foodNotifier" (or the updated hostname).
- Access the status by sending a GET request to the root ("/") endpoint.
- e.g. by using the the [Food Notifier Client](#food-notifier-client)

### API Endpoint

- Endpoint: /
- Method: HTTP GET
- Response Format: JSON
- Example Response:
```json
{"status":true}
```

### Additional Notes

- The server runs on port 80 by default.
- The button state is debounced to prevent false readings.
- The LED on the ESP8266 toggles based on the button press, reflecting the current status.
- Optionally, the updated status is printed to the Serial Monitor.

## Food Notifier Client

The Food Notifier Client connects to a Wi-Fi network and makes periodic HTTP GET requests to the Food Notifier Server by hostname. The project is designed to notify the user of specific conditions using an RGB LED.

### Table of Contents

- Requirements
- Setup
- Usage
- Code Structure
- Contributing
- License

### Requirements

To use the Food Notifier Client, you'll need the following components:

- ESP8266-based microcontroller (e.g., Wemos D1 Mini)
- RGB LED (e.g., WS2812B) connected to the microcontroller
- Wi-Fi network credentials (SSID and password)
- A server that responds to HTTP GET requests with a JSON payload containing a "status" field (e.g. the Food Notifier Server)

### Setup

1. Clone this repository to your local machine.
2. Open the Arduino IDE and install the required libraries: ESP8266WiFi, ESP8266HTTPClient, ArduinoJson, and Adafruit_NeoPixel.
3. Modify the arduino_secrets.h file with your Wi-Fi credentials (SSID and password).
4. Update the serverHostname variable with the hostname or IP address of your server.
5. Connect the RGB LED to the specified pin (e.g., D2) on the microcontroller.
6. Upload the code to your ESP8266-based microcontroller.

### Example setup

1. Battery shield and battery for remote use
2. RGB LED shield for a clean stack
3. Switch to connect/disconnect pin D0 with RST. If they're not connected, it won't wake up from deep sleep. This turns the device effectively off.
4. Print enclosure using a 3D printer.

<img src="foodNotifierClient/foodNotifierClient_bb.png" width="40%"> <img src="foodNotifierClient/foodNotifierClient_schem.png" width="40%">
<img src="foodNotifierClient/foodNotifierClientCase.png" width="40%">

### Usage

Once the code is uploaded to the microcontroller and connected to the Wi-Fi network, the Food Notifier Client will periodically make HTTP GET requests to the specified server. The RGB LED will change colors based on the server response:

- Green: Successful HTTP request with a true "status" field in the JSON response.
- Red: Failed HTTP request or false "status" field in the JSON response.

Additionals:
- The green LED will blink to grab the user's attention after a certain number of successful requests.
- If the server cannot be reached, it is assumed that the server is offline. The Client will go to deep sleep before making the next attempt.

### Code Structure

The code is structured into several functions for better readability and maintainability:

- setup: Initializes serial communication, NeoPixel library, and connects to Wi-Fi.
- loop: Main loop that makes periodic HTTP GET requests and handles LED colors accordingly.
- connectToWiFi: Function to connect to the Wi-Fi network.
- handleSuccessfulRequest: Function to handle a successful HTTP request.
- handleFailedRequest: Function to handle a failed HTTP request.
- setColor: Function to set the color of the RGB LED.
- makeGetRequest: Function to make an HTTP GET request to the server.
- handleFailedHTTP: Function to handle specific cases of failed HTTP requests.
- blinkLED: Function to blink the LED for attention.

### Contributing

Contributions to the Food Notifier project are welcome. Please follow the standard guidelines for contributions and submit a pull request.
License

This project is licensed under the MIT License - see the LICENSE file for details.