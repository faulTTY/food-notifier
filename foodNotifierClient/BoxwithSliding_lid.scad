// This is a modified version of https://www.thingiverse.com/thing:468917

width = 32;
length = 53;
height = 31;
wall_thickness = 2;
//ensure that the edge roundness is greater than the wall thickness
edge_roundness = 3;
tolerance = 0.1;

$fn = 20;

/* [Hidden] */

//ignore!
inner_box_dimentions = [length,width,height];

difference ()
{

	hull()
	{
		addOutterCorners(0,0);
		addOutterCorners(1,0);
		addOutterCorners(0,1);
		addOutterCorners(1,1);
	}
	
	translate([0,0,wall_thickness])
	hull()
	{
		addInnerCorners(0,0);
		addInnerCorners(1,0);
		addInnerCorners(0,1);
		addInnerCorners(1,1);
	}

	translate([0,0,inner_box_dimentions[2]+0.1])
	hull()
	{
		addLidCorners(0,0);
		addLidCorners(1,0);
		addLidCorners(0,1);
		addLidCorners(1,1);
	}

	translate([inner_box_dimentions[0]-wall_thickness,0,inner_box_dimentions[2]+0.1])
	cube([wall_thickness,inner_box_dimentions[1],wall_thickness]);
    
    // holes
    translate([length-3,15.5,10]) cube([5,12,7]);
    translate([length,width/2+1,22]) cube([5,10,4],true);
    translate([8,0.5,height+0.1]) cube([15,5,5]);
    translate([38,0.5,height+0.1]) cube([15,5,5]);
}

// extra box inside box (spacer)
translate([8,1,21]) cube([length-35-10,width-2,height-21]);


difference ()
{
	translate([0,inner_box_dimentions[1]+2,0])
	hull()
	{
		Lid(0,0);
		Lid(1,0);
		Lid(0,1);
		Lid(1,1);
	}

	//translate([20,inner_box_dimentions[1]*1.5+2,0])
	//rotate([0,90,0]);

    // holes
	translate([0,inner_box_dimentions[1]+2,0]) translate([36,width/2,0]) cube([6,6,5],true);
    translate([0,inner_box_dimentions[1]+2,0]) translate([15,0,-1]) cube([17,6,5]);
}

// Add custom text to lid
translate([0,inner_box_dimentions[1]+2,2]) myText();
module myText()
{
    linear_extrude(.6)
    translate([5, width*3/4, 0]) text( "Food Notifier", size= 4);
}

translate([0,inner_box_dimentions[1]+2,2]) onOffText();
module onOffText()
{
    linear_extrude(.6)
    translate([20, width/4, 0]) text( "ON / OFF", size= 2);
}


module addOutterCorners(x = 0, y = 0)
{
	translate([(inner_box_dimentions[0] - edge_roundness*2 + 0.1)*x,(inner_box_dimentions[1] - edge_roundness*2 +0.1)*y,0] + [edge_roundness,edge_roundness,0])

	cylinder(inner_box_dimentions[2]+wall_thickness,edge_roundness,edge_roundness);
	
	echo((inner_box_dimentions[0] - edge_roundness)*x);
	echo((inner_box_dimentions[1] - edge_roundness)*y);
}

module addInnerCorners(x = 0, y = 0)
{
	translate([(inner_box_dimentions[0] - edge_roundness*2 + 0.1)*x,(inner_box_dimentions[1] - edge_roundness*2 +0.1)*y,0] + [edge_roundness,edge_roundness,0])

	cylinder(inner_box_dimentions[2],edge_roundness-wall_thickness,edge_roundness-wall_thickness);
	
	echo((inner_box_dimentions[0] - edge_roundness)*x);
	echo((inner_box_dimentions[1] - edge_roundness)*y);
}

module addLidCorners(x = 0, y = 0)
{
	translate([(inner_box_dimentions[0] - edge_roundness*2 - 0.1 +wall_thickness)*x,(inner_box_dimentions[1] - edge_roundness*2 +0.1)*y,0] + [edge_roundness,edge_roundness,0])

	cylinder(wall_thickness,edge_roundness-wall_thickness+1.5,edge_roundness-wall_thickness+0.5);
	
	echo((inner_box_dimentions[0] - edge_roundness)*x);
	echo((inner_box_dimentions[1] - edge_roundness)*y);
}

module Lid(x = 0, y = 0)
{
	translate([(inner_box_dimentions[0] - edge_roundness*2 - 0.1 +wall_thickness-2)*x,(inner_box_dimentions[1] - edge_roundness*2 +0.1)*y,0] + [edge_roundness,edge_roundness,0])

	cylinder(wall_thickness,edge_roundness-wall_thickness+1.5-tolerance,edge_roundness-wall_thickness+0.5-tolerance);
	
	echo((inner_box_dimentions[0] - edge_roundness)*x);
	echo((inner_box_dimentions[1] - edge_roundness)*y);
}