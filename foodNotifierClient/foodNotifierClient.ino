#include "arduino_secrets.h"
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <Adafruit_NeoPixel.h>

const char* serverHostname = "foodNotifier";
const char ssid[] = SECRET_SSID;
const char password[] = SECRET_PASS;
const int ledPin = D2;

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(1, ledPin, NEO_GRB + NEO_KHZ800);

int isOnCounter = 0;

void setup() {
  Serial.begin(115200);
  pixels.begin();
  connectToWiFi();
}

void loop() {
  if (makeGetRequest()) {
    handleSuccessfulRequest();
  } else {
    handleFailedRequest();
  }
  delay(30000); // check server status every 30 seconds
}

void connectToWiFi() {
  WiFi.begin(ssid, password);
  Serial.println("Connecting to WiFi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("Connected to WiFi");
}

void handleSuccessfulRequest() {
  setColor(0, 30, 0, 0);  // Green
  handleBlinkIteration(0, 30, 0); // Get users attention
}

void handleFailedRequest() {
  setColor(30, 0, 0, 0);  // Red
}

void setColor(int redValue, int greenValue, int blueValue, int delayValue) {
  pixels.setPixelColor(0, pixels.Color(redValue, greenValue, blueValue)); 
  pixels.show();
  delay(delayValue);
}

bool makeGetRequest() {
  WiFiClient client;
  HTTPClient http;

  String url = "http://" + String(serverHostname);
  http.begin(client, url);
  http.setTimeout(5000);

  int httpCode = http.GET();
  if (httpCode > 0 && httpCode == HTTP_CODE_OK) {
    String payload = http.getString();
    Serial.println("Response: " + payload);

    DynamicJsonDocument jsonDoc(1024);
    deserializeJson(jsonDoc, payload);

    bool status = jsonDoc["status"];
    return status;
  } else {
    handleFailedHTTP(httpCode);
    return false;
  }
}

void handleFailedHTTP(int httpCode) {
  setColor(0, 0, 0, 100);
  Serial.println("HTTP request failed");

  if (httpCode == HTTPC_ERROR_CONNECTION_REFUSED) {
    ESP.deepSleep(300e6);
  }
}

void handleBlinkIteration(int redValue, int greenValue, int blueValue) {
  if (isOnCounter == 2) {
      blinkLED(redValue, greenValue, blueValue);
      isOnCounter = 0;
  } else {
      isOnCounter++;
  }
}

void blinkLED(int redValue, int greenValue, int blueValue) {
  for (int i = 0; i < 5; i++) {
    setColor(0, 0, 0, 100);
    setColor(redValue, greenValue, blueValue, 100);
  }
}
